package com.company;

import com.google.gson.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

public class Connector {
    URL url;
    HttpURLConnection con;

    public Connector(String searchQuery) throws IOException {
        if (searchQuery.length() == 0) {
            url = new URL("https://quote-garden.herokuapp.com/quotes/all");
        } else {
            url = new URL("https://quote-garden.herokuapp.com/quotes/search/" + searchQuery);
        }
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");

        con.setRequestProperty("Content-Type", "application/json");

        con.setConnectTimeout(5000);
        con.setReadTimeout(5000);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream())
        );
        String inputLine;
        StringBuffer content = new StringBuffer();

        inputLine = in.readLine();
        content.append(inputLine);
        Quote[] quoteList = getFirst20Quotes(inputLine);

        for (Quote quote : quoteList) {
            System.out.println(quote.toString().replace(searchQuery, searchQuery.toUpperCase()));

        }


        in.close();
        con.disconnect();
    }

    private Quote[] getFirst20Quotes(String APIresult) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        Result result = gson.fromJson(APIresult, Result.class);
        return result.getFirst20Quotes();
    }



}

class Quote {
    private String quoteText;
    private String _id;
    private String quoteAuthor;

    @Override
    public String toString() {
        return
                "'" + quoteText + '\'' +
                " -" + quoteAuthor;
    }
}

class Result {
    private int count;
    private Quote[] results;

    public Quote[] getFirst20Quotes() {
        if( results.length >= 20 ){
            return Arrays.copyOfRange(results, 0, 20);
        }
        return Arrays.copyOfRange(results, 0, results.length);
    }
}
