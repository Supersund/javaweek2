package com.company;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to this Quote-searcher! Choose a word to search for and get awesome quotes!");
        System.out.print("Searchword: ");
	    Connector connector = new Connector(scanner.nextLine());
    }
}
