package com.company;

public interface Walkable extends Moveable {
    void walk();
}
