package com.company;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Random;

public abstract class Animal {
    protected List<Class<Moveable>> movements;

    public void doRandomMovement() throws InvocationTargetException, IllegalAccessException {
        if (movements.size() == 0) {
            System.out.println("cannot do any move");
        } else {
            Class<Moveable> movement = movements.get(new Random().nextInt(movements.size()));
            movement.getMethods()[0].invoke(this, null);
        }

    }

    public void printLegalMovements() {
        System.out.println(this.getClass().getSimpleName() + " can: ");
        for (Class<Moveable> movement : movements) {
            System.out.print(movement.getMethods()[0].getName() + " ");
        }
        System.out.println();
    }
}
