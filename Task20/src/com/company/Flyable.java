package com.company;

public interface Flyable extends Moveable{
    void fly();
}
