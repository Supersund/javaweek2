package com.company;

import java.util.ArrayList;

public class Bird extends Omnivore implements Flyable, Walkable{

    public Bird() {
        this.movements = new ArrayList<>();
        Class<Moveable>[] interfaces = (Class<Moveable>[]) this.getClass().getInterfaces();

        for (Class<Moveable> interF : interfaces) {
            this.movements.add(interF);
        }
    }

    @Override
    public void fly() {
        System.out.println("The bird flies");
    }

    @Override
    public void walk() {
        System.out.println("The bird walks");
    }
}
