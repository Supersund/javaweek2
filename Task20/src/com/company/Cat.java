package com.company;

import java.util.ArrayList;

public class Cat extends Carnivore implements Walkable, Runable, Climbable {

    public Cat() {
        this.movements = new ArrayList<>();
        Class<Moveable>[] interfaces = (Class<Moveable>[]) this.getClass().getInterfaces();

        for (Class<Moveable> interF : interfaces) {
            this.movements.add(interF);
        }
    }
    @Override
    public void climb() {
        System.out.println("The cat climbs");
    }

    @Override
    public void run() {
        System.out.println("The cats runs");
    }

    @Override
    public void walk() {
        System.out.println("The cat walks");
    }
}
