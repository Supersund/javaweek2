package com.company;

import java.util.ArrayList;

public class Dog extends Carnivore implements Runable, Walkable {

    public Dog() {
        this.movements = new ArrayList<>();
        Class<Moveable>[] interfaces = (Class<Moveable>[]) this.getClass().getInterfaces();

        for (Class<Moveable> interF : interfaces) {
            this.movements.add(interF);
        }
    }

    @Override
    public void run() {
        System.out.println("The dog runs");
    }

    @Override
    public void walk() {
        System.out.println("The dog walks");
    }
}
