package com.company;

public interface Climbable extends Moveable{
    void climb();
}
