package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.*;
import java.util.function.Function;

public class Parser {

    private static final Set<Character> setOfStringChars = new HashSet(Arrays.asList('"', '\''));
    private static final Set<Character> setOfChars = new HashSet(Arrays.asList('(', '{', '[', '\"', '\''));
    private static final Map<Character, Character> mapOfBrackets = Map.of(')', '(', '}', '{', ']', '[');


    public static boolean parseText(String text) {
        ArrayList<String> results = parsePartOfText(text, new ArrayList<>());
        return results.isEmpty();
    }
    /*Reading file line by line storing a list of different opening brackets called symbols*/
    public static boolean parseFile(File file) {
        ArrayList<String> symbols = new ArrayList<>();
        Scanner in = new Scanner(System.in);
        try {
            in = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        while (in.hasNextLine()) {
            String nextLine = in.nextLine();

            symbols = parsePartOfText(nextLine, symbols);
            //checking if symbols has values to avoid index out of bounds,
            // if the last symbol in symbols list is a "f" returning false
            if (!symbols.isEmpty() && symbols.get(symbols.size() - 1).equals("f")) {
                return false;
            }
        }
        //if there are no more chars in the list all opening brackets had a closing bracket.
        return symbols.isEmpty();
    }

    private static ArrayList<String> parsePartOfText(String text, ArrayList<String> symbols) {
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);

            //Looking for " and ' in Strings.
            if (!symbols.isEmpty() && setOfStringChars.contains(symbols.get(symbols.size() - 1).charAt(0))) {
                if (symbols.get(symbols.size() - 1).equals(String.valueOf(c))) {
                    symbols.remove(symbols.size() - 1);
                }
                if (c == '\\' && (i + 1) < text.length() && (text.charAt(i + 1) == symbols.get(symbols.size()-1).charAt(0) || text.charAt(i+1)=='\\')) {
                   ++i;
                }
                continue;
            }
            //Looking for //. If they are found we return the list of symbols and continue on the next line.
            if (c == '/' && i + 1 < text.length() && text.charAt(i + 1) == '/') {
                return symbols;
            }
            //We have found a opening bracket and are storing it in the symbols list
            if (setOfChars.contains(c)) {
                symbols.add(String.valueOf(c));
                return parsePartOfText(text.substring(i + 1), symbols);
            }
            //we have found a closing bracket.
            if (mapOfBrackets.containsKey(c)) {
                //There are no opening bracket and "f" for false is returned
                if (symbols.isEmpty()) {
                    return new ArrayList<String>(Arrays.asList("f"));
                }
                //The closing bracket does not match the last opening bracket, returning "f" for false
                if (!symbols.get(symbols.size() - 1).equals(String.valueOf(mapOfBrackets.get(c)))) {
                    return new ArrayList<String>(Arrays.asList("f"));
                }
                //The correct closing bracket has been found and the last opening bracket is removed.
                symbols.remove(symbols.size() - 1);
                return parsePartOfText(text.substring(i + 1), symbols);
            }

        }

        return symbols;
    }
}
