### Written by Bror and Jørgen

In our solution we are going through every char in the File.
For every opening bracket ({[, we store the char in a list. Then the next time
we find a char that is a bracket we see if its a matching closing bracket. If it is we delete the
last opening bracket that we found from the list, if not we return false. If we by the end have
an empty array of with no brackets the brackets are matching.

We have also solved it for double quotes "" and single quotes '' and for one
line comments //.


We started programming with no clear rules of when we should switch who was programming.
We used the first half hour discussing our different views on the problem and how we 
wanted to solve it. This made it very easy to cooperate in the start as we both had the same idea
as how to aproach the problem. At the start of the assignment we changed who was in control
of the keyboard every time a test failed or if a new problem arrised. But by the end it was whoever
saw a solution and could explain it to the other that got the controls. 