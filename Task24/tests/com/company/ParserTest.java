package com.company;

import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class ParserTest {

    @Test
    public void testSimpleString() {
        assertTrue(Parser.parseText("{}"));
        assertFalse(Parser.parseText("(()"));
        assertTrue(Parser.parseText("(a(b))c"));
        assertFalse(Parser.parseText("())"));
        assertTrue(Parser.parseText("(blabla{woop[lool]}dfd)"));
        assertFalse(Parser.parseText("(blablawoop[lool]}dfd])"));
    }

    @Test
    public void testFileParsing() {
        assertTrue(Parser.parseFile(new File("src/com/company/Parser.java")));
    }

    @Test
    public void testTestFileParsing() {
        assertTrue(Parser.parseFile(new File("tests/com/company/ParserTest.java")));
    }


}