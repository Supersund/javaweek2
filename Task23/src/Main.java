import java.sql.*;

public class Main {

    private Connection connect() throws ClassNotFoundException {
        Class.forName("org.sqlite.JDBC");
        String url = "jdbc:sqlite:C:\\Users\\jssund\\Documents\\Week 6\\Task23\\resources\\Northwind_small.sqlite";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    public void selectAll() {
        String sql = "SELECT * FROM Customer";
        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {

            // loop through the result set
            while (rs.next()) {
                System.out.println(rs.getString("id") + "\t" +
                        rs.getString("CompanyName") + "\t" +
                        rs.getString("ContactName"));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) throws ClassNotFoundException {
        Main main = new Main();
        main.selectAll();
    }
}
