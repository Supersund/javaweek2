import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

class PetRockTest {
    private PetRock rock;

    @BeforeEach
    public void setUp() {
        rock = new PetRock("Rocky");
    }

    @Test
    void getName() {
        assertEquals("Rocky", rock.getName());
    }

    @Test
    void testUnhappyToStart() {
        assertFalse(rock.isHappy());
    }

    @Test
    void testHappyAfterPlay() {
        rock.playWithRock();
        assertTrue(rock.isHappy());
    }

    @Test
    void testPrintHappyMessageWhenUnhappy() {
        assertThrows(IllegalStateException.class, () -> {
            rock.getHappyMessage();
        });
    }

    @Test
    void testPrintHappyMessageWhenHappy() {
        rock.playWithRock();
        assertEquals("I'm happy!", rock.getHappyMessage());
    }

    @Test
    void testFavNum() {
        assertEquals(42, rock.getFavNumber());
    }

    @Test
    void testEmptyNameConstructor() {
        assertThrows(IllegalArgumentException.class, () -> {
            new PetRock("");
        });
    }

    @Test
    void waitForHappy () {
        assertTimeoutPreemptively(Duration.ofMillis(500),() -> {
            rock.waitTillHappy();
        });
    }

}