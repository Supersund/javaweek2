public class PetRock {

    private String name;
    private boolean happy = false;

    public String getName() {
        return name;
    }

    public PetRock(String name) {
        if (name.isEmpty()) {
            throw  new IllegalArgumentException();
        }
        this.name = name;
    }

    public boolean isHappy() {
        return happy;
    }

    public void playWithRock() {
        this.happy = true;
    }

    public String getHappyMessage() {
        if (!happy) {
            throw new IllegalStateException();
        }
        return ("I'm happy!");
    }

    public int getFavNumber() {
        return 42;
    }

    public void waitTillHappy() {
        while (!happy) {

        }
    }
}
