trait Walkable {
  def walk(species: String): Unit = {
    println(species+" walks")
  }
}
