class Bird extends Omnivore("Bird") with Walkable with Flyable {
  var interfaces : Array[Class[_]] = this.getClass.getInterfaces
  Thread.sleep(40)
  var movements : Array[Movement] = new Array[Movement](interfaces.length)
  Thread.sleep(40)
  interfaces.zipWithIndex.foreach { case(item, index) =>

    item.getMethods.foreach(method => {
      if (!method.getName.contains("$")) {
        movements.update(index, new Movement(method, this))
      }
    })
  }

}
