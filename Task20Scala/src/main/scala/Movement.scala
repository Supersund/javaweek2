import java.lang.reflect.Method

class Movement(var method: Method, var myObject: Animal) {
  def performMovement() {
    method.invoke(myObject, myObject.species)
  }
}
