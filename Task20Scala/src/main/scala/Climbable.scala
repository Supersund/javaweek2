trait Climbable {
  def climb(species: String): Unit = {
    println(species+" climbs")
  }
}
