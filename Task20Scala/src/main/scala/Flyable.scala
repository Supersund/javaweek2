trait Flyable {
  def fly(species: String): Unit = {
    println(species+" flies")
  }
}
