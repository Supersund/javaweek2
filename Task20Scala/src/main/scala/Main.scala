object Main {
  def main(args: Array[String]) = {
    val cat : Cat = new Cat()
    cat.doRandomMove()
    cat.printAllMoves()
  }
}
