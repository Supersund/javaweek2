import scala.util.Random

abstract class Animal(var species: String) {

  var movements : Array[Movement]

  def printAllMoves(): Unit = {
    println(species+ " can do the following movements:")
    movements.foreach(movement => {
      print(movement.method.getName()+" ")
    })
  }

  def doRandomMove(): Unit = {
    movements.apply(new Random().nextInt(movements.size)).performMovement()
  }
}
